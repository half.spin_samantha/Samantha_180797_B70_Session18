<?php

class Person{


    public $myProperty = "A1";
    public $myP1 = "A2",$myP2 = "A3",$myP3 = "A4";

    public function __sleep()
    {
        // TODO: Implement __sleep() method.

        echo "I'm inside ".__METHOD__."<br>";

        return array('myP1','myP3');

    }



    public function __wakeup()
    {
        // TODO: Implement __wakeup() method.

        //Task : DB connection
        echo "Database connected again<br>";

    }



    public static function __callStatic($name, $arguments)
    {
        // TODO: Implement __callStatic() method.

        echo "<br>";
        echo "I'm inside ".__METHOD__."<br>";
        echo $name."   "."<br>";

        echo "<pre>";
        var_dump($arguments);
        echo "</pre>";

    }



    public static function doSomethingStatic(){

        echo "I'm inside ".__METHOD__."<br>";

    }



    public function __call($name, $arguments)
    {
        // TODO: Implement __call() method.

        echo "<br>";
        echo "I'm inside ".__METHOD__."<br>";
        echo $name."   "."<br>";

        echo "<pre>";
        var_dump($arguments);
        echo "</pre>";
    }




    public function __unset($name)
    {
        // TODO: Implement __unset() method.

        echo "<br>";
        echo "I'm inside ".__METHOD__."<br>";
        echo $name."   "."<br>";
    }




    public function __isset($name)
    {
        // TODO: Implement __isset() method.

        echo "<br>";
        echo "I'm inside ".__METHOD__."<br>";
        echo $name."   "."<br>";
    }





    public function __set($name, $value)
    {
        // TODO: Implement __set() method.

        echo "<br>";
        echo "I'm inside ".__METHOD__."<br>";
        echo $name."   ".$value."<br>";
    }




    public function __get($name)
    {
        // TODO: Implement __get() method.

        echo "<br>";
        echo "I'm inside ".__METHOD__."<br>";
        echo $name."   "."<br>";

    }





    public function __construct()
    {
        //task for db connection
        echo "Database connection successful<br>";
    }




    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        echo "Maaf kore diyen.".__METHOD__."<br>";
    }





    public function doSomething(){

        echo "I'm doing something from ".__METHOD__."<br>";

    }

}


$objPerson1 = new Person();
$objPerson2 = new Person();
$objPerson3 = new Person();

unset($objPerson2);
$objPerson1->doSomething();
$objPerson1->aPropertyThatDoesNotExist = "Hello World";

echo "";


if(isset($objPerson1->aPropertyThatDoesNotExist)){

    echo "I am insite an object that does not exist<br>";


}else{

}

unset($objPerson1->aPropertyThatDoesNotExist);


$objPerson1->doingSomething("Hello World",123,true,12.3);


Person::doSomethingStatic();


Person::doSomethingDoesNotExist("gate",34);



$myVar = serialize($objPerson1);

var_dump($myVar);

$myPerson1Object = unserialize($myVar);


var_dump($myPerson1Object);


echo "<hr>";




